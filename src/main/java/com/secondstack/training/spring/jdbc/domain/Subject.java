package com.secondstack.training.spring.jdbc.domain;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/23/13
 * Time: 8:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class Subject {
    private Integer id;
    private String code;
    private String name;
    private Integer sks;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSks() {
        return sks;
    }

    public void setSks(Integer sks) {
        this.sks = sks;
    }
}
